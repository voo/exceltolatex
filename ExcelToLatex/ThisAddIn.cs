﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using System.Threading;
using System.Globalization;

namespace ExcelToLatex {

public partial class ThisAddIn {

	internal Exception Error(string errormsg) {
		Console.Error.WriteLine(errormsg);
		// We are lazy
		return new ApplicationException(errormsg);
	}

	/// <summary>
	/// Converts given selection of excel table to its corresponding LaTeX representation.
	/// Assumes that if multiple selections exist, that they all have the same number of rows,
	/// otherwise throws error.
	/// </summary>
	internal void Convert() {
		IList<ICollection<Excel.Range>> cells = null;
		int nrRows = -1;
		foreach (var selection in GetOrderedSelections()) {
			var cellRegion = GetCellsFromSelection(selection);
			if (nrRows == -1) {
				// First iteration
				nrRows = cellRegion.Count;
				cells = cellRegion;
			} else {
				if (cellRegion.Count != nrRows) 
					throw Error("Multiple selection with different row counts not allowed.");
				AddCells(cells, cellRegion);
			}
		}
		var representation = new TableRepresentation(cells);
		System.Windows.Forms.Clipboard.SetText(representation.GetLatexRepresentation());
	}

	/// <summary>
	/// Return all selected ranges sorted by their startcolumn, followed by their startrow.
	/// </summary>
	/// <returns></returns>
	IEnumerable<Excel.Range> GetOrderedSelections() {
		var areas = Application.Selection.Areas;
		var ranges = new List<Excel.Range>();
		foreach (var range in areas)
			ranges.Add(range);
		// Order ranges correctly, first by column, then by row
		Func<string, dynamic> parseCell = cell => {
			var parts = cell.Split(new[] { '$', ':' }, StringSplitOptions.RemoveEmptyEntries);
			return new { Column = parts[0], Row = int.Parse(parts[1], CultureInfo.InvariantCulture) };
		};
		return ranges.OrderBy(x => {
			return parseCell(x.Address).Column.Length;
		}).ThenBy(x => {
			return parseCell(x.Address).Column;
		}).ThenBy(x => {
			return parseCell(x.Address).Row;
		});
	}

	/// <summary>
	/// Adds newCells to cells collection. Assumes that we have the same number of rows in both collections.
	/// If cells is 5x10 and newCells is 5x7 after AddCells() cells should be 5x17.
	/// Example:
	/// cells:
	/// a1, b1
	/// a2, b2
	/// newCells
	/// c1
	/// c2
	/// cells afterwards: 
	/// a1, b1, c1
	/// a2, b2, c2
	/// </summary>
	/// <param name="cells">Collection to which newCells should be added</param>
	/// <param name="newCells"></param>
	static void AddCells(IList<ICollection<Excel.Range>> cells, IList<ICollection<Excel.Range>> newCells) {
		for (int i = 0; i < cells.Count; ++i) {
			cells[i] = cells[i].Concat(newCells[i]).ToList();
		}
	}

	/// <summary>
	/// Returns a collection containing all cells of selection in correct 2d order.
	/// </summary>
	/// <param name="selection"></param>
	/// <returns></returns>
	static IList<ICollection<Excel.Range>> GetCellsFromSelection(Excel.Range selection) {
		return (from Excel.Range col in selection.Rows select col.Cells.Cast<Excel.Range>().ToList()).
			Cast<ICollection<Excel.Range>>().ToList();
	}
	
	void ThisAddIn_Startup(object sender, System.EventArgs e) {
	}

    void ThisAddIn_Shutdown(object sender, System.EventArgs e) {
    }

    #region VSTO generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InternalStartup()
    {
        this.Startup += new System.EventHandler(ThisAddIn_Startup);
        this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
    }
        
    #endregion
}
}
