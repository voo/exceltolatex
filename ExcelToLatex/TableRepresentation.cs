﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelToLatex {
internal class TableRepresentation {
	readonly ICollection<ICollection<Excel.Range>> cells;
	const double BLACK_COLOR = 0.0;
	const double WHITE_COLOR = 16777215.0;

	// True if the cell next to the current one had a special alignment in this line.
	// Necessary to avoid having double sized bars between two cells with special alignment.
	bool previousCellInLineSpecialAligned = false;

	public TableRepresentation(ICollection<ICollection<Excel.Range>> cells) {
		this.cells = cells;
	}

	/// <summary>
	/// Returns LaTeX representation of the 2d collection of excel cells including header and footer.
	/// </summary>
	/// <returns></returns>
	public string GetLatexRepresentation() {
		// TODO: Think about allowing table header to have specified size (maybe corresponding to column lengths?) 
		// instead of just fitting. 
		var result = new StringBuilder();
		result.Append(GetHeader());
		result.AppendLine(@"\hline");
		foreach (var col in cells) {
			foreach (var cell in col) {
				result.Append(GetCellRepresentation(cell));
			}
			// Remove last &
			result.Remove(result.Length - 1, 1);
			result.Append(@"\\");
			result.AppendLine(@"\hline");
			previousCellInLineSpecialAligned = false;
		}
		result.AppendLine(GetFooter());
		return result.ToString();
	}

	/// <summary>
	/// Creates a LaTeX header that left aligns all cells and puts | between each column.
	/// We should probably allow this behavior to be changed to use the actual excel border 
	/// in the future.
	/// </summary>
	/// <returns></returns>
	string GetHeader() {
		var result = new StringBuilder(@"\begin{tabular}{"); 
		for (int i = 0; i < cells.Count; i++) {
			result.Append("|l");
		}
		result.Append("|}");
		return result.ToString();
	}

	static string GetFooter() {
		return @"\end{tabular}";
	}

	/// <summary>
	/// Returns the LaTeX representation of cell, including the correct alignment (left, right, justified), 
	/// text color and background color.
	/// </summary>
	/// <param name="cell"></param>
	/// <returns></returns>
	string GetCellRepresentation(Excel.Range cell) {
		var result = new StringBuilder();
		int openBraces = 0;
		// IMPORTANT: Alignment has to be done first.
		if (HasSpecialAlignment(cell)) {
			result.Append(GetAlignmentRepresentation(cell));
			previousCellInLineSpecialAligned = true;
			openBraces++;
		} else {
			previousCellInLineSpecialAligned = false;
		}
		if (cell.Font.Bold) {
			result.Append(@"\textbf{");
			openBraces++;
		}
		if (cell.Font.Italic) {
			result.Append(@"\textit{");
			openBraces++;
		}
		if (cell.Font.Color != BLACK_COLOR) {
			result.Append(GetTextColorRepresentation(cell));
			openBraces++;
		}
		if (cell.Interior.Color != WHITE_COLOR) {
			result.Append(GetBackgroundColorRepresentation(cell));
		}
		result.Append(cell.Value2 ?? "");
		for (int i = 0; i < openBraces; i++) {
			result.Append("}");
		}
		result.Append("&");
		return result.ToString();
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="cell"></param>
	/// <returns>True if cell is not left-aligned.</returns>
	static bool HasSpecialAlignment(Excel.Range cell) {
		return !(cell.HorizontalAlignment == (int)Excel.XlHAlign.xlHAlignLeft || 
			cell.HorizontalAlignment == (int)Excel.XlHAlign.xlHAlignGeneral);
	}

	string GetAlignmentRepresentation(Excel.Range cell) {
		// If we have two different alignments side by side we would get a double sized seperation
		// line, so we have to check if the cell to left had a special alignment itself and if yes
		// don't specify the left |.
		string leftBar = previousCellInLineSpecialAligned ? "" : "|";
		char alignment;
		if (cell.HorizontalAlignment == (int)Excel.XlHAlign.xlHAlignCenter) {
			alignment = 'c';
		} else if (cell.HorizontalAlignment == (int)Excel.XlHAlign.xlHAlignRight) {
			alignment = 'r';
		} else {
			throw Globals.ThisAddIn.Error("Unknown horizontal alignment.");
		}
		return @"\multicolumn{1}{" + leftBar + alignment + "|}{";
	}

	static string GetTextColorRepresentation(Excel.Range cell) {
		Color c = ExcelColorToRgb(cell.Font.Color);
		return string.Format(CultureInfo.InvariantCulture, @"\textcolor[rgb]{{ {0:F3},{1:F3},{2:F3} }}{{",
			c.R / 255.0, c.G / 255.0, c.B / 255.0);
	}

	static string GetBackgroundColorRepresentation(Excel.Range cell) {
		Color c = ExcelColorToRgb(cell.Interior.Color);
		return string.Format(CultureInfo.InvariantCulture, @"\cellcolor[rgb]{{ {0:F3},{1:F3},{2:F3} }}", 
			c.R / 255.0, c.G / 255.0, c.B / 255.0);
	}

	static Color ExcelColorToRgb(double colorValue) {
		// For some strange reason both the background and text color of cells store the RGB values as a double  
		// and not an int. This works because we can represent all integers < 2**53 precisely in a 
		// IEEE-754 double, but still seems like a bug to me.
		int rgb = (int)colorValue;
		int r = rgb & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = (rgb >> 16) & 0xff;
		// Cannot use Color.FromArgb(int32) because rgb channels are in the wrong order.
		return Color.FromArgb(r, g, b);
	}

	public override string ToString() {
		return GetLatexRepresentation();
	}

}

}
